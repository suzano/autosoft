var _____WB$wombat$assign$function_____ = function(name) {return (self._wb_wombat && self._wb_wombat.local_init && self._wb_wombat.local_init(name)) || self[name]; };
if (!self.__WB_pmw) { self.__WB_pmw = function(obj) { this.__WB_source = obj; return this; } }
{
  let window = _____WB$wombat$assign$function_____("window");
  let self = _____WB$wombat$assign$function_____("self");
  let document = _____WB$wombat$assign$function_____("document");
  let location = _____WB$wombat$assign$function_____("location");
  let top = _____WB$wombat$assign$function_____("top");
  let parent = _____WB$wombat$assign$function_____("parent");
  let frames = _____WB$wombat$assign$function_____("frames");
  let opener = _____WB$wombat$assign$function_____("opener");

var JCaption=new Class({initialize:function(b){this.selector=b;var a=$$(b);a.each(function(c){this.createCaption(c)},this)},createCaption:function(c){var b=document.createTextNode(c.title);var a=document.createElement("div");var e=document.createElement("p");var d=c.getAttribute("width");var f=c.getAttribute("align");if(!d){d=c.width}if(!f){f=c.getStyle("float")}if(!f){f=c.style.styleFloat}if(f==""||!f){f="none"}e.appendChild(b);e.className=this.selector.replace(".","_");c.parentNode.insertBefore(a,c);a.appendChild(c);if(c.title!=""){a.appendChild(e)}a.className=this.selector.replace(".","_");a.className=a.className+" "+f;a.setAttribute("style","float:"+f);a.style.width=d+"px"}});document.caption=null;window.addEvent("load",function(){var a=new JCaption("img.caption");document.caption=a});

}
/*
     FILE ARCHIVED ON 23:02:42 Jan 29, 2019 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 20:08:54 Aug 19, 2021.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  captures_list: 434.357
  exclusion.robots: 284.533
  exclusion.robots.policy: 284.527
  xauthn.identify: 213.943
  xauthn.chkprivs: 70.376
  RedisCDXSource: 6.234
  esindex: 0.007
  LoadShardBlock: 126.907 (3)
  PetaboxLoader3.datanode: 112.399 (4)
  CDXLines.iter: 14.616 (3)
  load_resource: 89.225
  PetaboxLoader3.resolve: 53.463
*/