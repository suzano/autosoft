---
layout: page-2014
permalink: "program-2014"
title: "Program"
categories: "2014"
---

<div id="wrapper2">
	<div id="main" role="main">
		<div id="system-message-container">
		</div>
		<div class="item-page">
			<h2>
				Program </h2>
			<p>&nbsp;</p>
			<table border="1" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td valign="top" width="688">
							<p align="center"><strong><strong><strong>Opening Session</strong></strong></strong></p>
						</td>
					</tr>
				</tbody>
			</table>
			<p><strong>8:50 - 9:05 Opening:</strong> Ingrid Nunes (UFRGS) and Mariela Inés Cortés (UECE)</p>
			<table border="0" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td valign="top" width="690">
							<p align="center"><strong><strong><strong>Invited Speaker 1</strong></strong></strong></p>
						</td>
					</tr>
				</tbody>
			</table>
			<p><strong>Chair: </strong>Ingrid Nunes</p>
			<p><strong>9:05 - 10:05 Invited Speaker:</strong> Michael Luck (King's College London)</p>
			<p><strong>Title: </strong>From Agents to Electronic Order via Norms</p>
			<table border="0" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td valign="top" width="690">
							<p align="center"><strong><strong>10:05 </strong><strong>–10:30: Technical
									</strong><strong>Session 1</strong></strong></p>
						</td>
					</tr>
				</tbody>
			</table>
			<p>&nbsp;<strong>Chair: </strong>Ingrid Nunes</p>
			<ul>
				<li><strong style="line-height: 1.3em;">10:05 – 10:30</strong><span style="line-height: 1.3em;"> - Uma
						plataforma para agentes normativos baseada no modelo Huginn<br></span></li>
				<ul>
					<li><span><span style="font-size: small;"><span style="line-height: normal;">Tiago Luiz Schmitz and
									Jomi Fred Hubner</span></span></span></li>
				</ul>
			</ul>
			<table border="1" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td valign="top" width="688">
							<p align="center"><span style="color: #999999;"><strong><strong><strong>Coffee Break
												10:30-11:00</strong></strong></strong></span></p>
						</td>
					</tr>
				</tbody>
			</table>
			<p>&nbsp;</p>
			<table border="0" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td valign="top" width="690">
							<p style="text-align: center;" align="center">
								<strong></strong><strong></strong><strong><strong><strong>10:30-12:20 Technical
										</strong><strong>Session 2</strong></strong></strong></p>
						</td>
					</tr>
				</tbody>
			</table>
			<p><strong>Chair: </strong><span>Jaime Sichman<br></span></p>
			<ul>
				<li><strong style="line-height: 1.3em;">11:00 – 11:25</strong><span style="line-height: 1.3em;"> - <span
							style="font-size: small;"><span style="line-height: normal;">Reengineering Network
								Resilience Strategies using the BDI Architecture</span></span></span></li>
				<ul>
					<li><span style="line-height: 1.3em;"><span style="font-size: small;"><span
									style="line-height: normal;">Ingrid Nunes and Alberto
									Schaeffer-Filho</span></span></span></li>
				</ul>
			</ul>
			<ul>
				<li><strong style="line-height: 1.3em;">11:25 – 11:50 -</strong><span style="line-height: 1.3em;"> <span
							style="font-size: small;"><span style="line-height: normal;">JAMDER 2.0: Support to
								Implementation of Normative Multi-Agent Systems</span></span><br></span></li>
				<ul>
					<li><span style="line-height: 1.3em;"><span style="font-size: small;"><span
									style="line-height: normal;">Robert Marinho, Emmanuel Sávio Silva Freire and Mariela
									Inés Cortés</span></span></span></li>
				</ul>
			</ul>
			<ul>
				<li><strong style="line-height: 1.3em;">11:50 – 12:15 -</strong><span style="line-height: 1.3em;"> <span
							style="font-size: small;"><span style="line-height: normal;">Fragmenting O-MaSE using the
								Medee Framework</span></span><br></span></li>
				<ul>
					<li><span style="line-height: 1.3em;"><span style="font-size: small;"><span
									style="line-height: normal;">Felipe Cardoso Resende, Sara Casare, Anarosa Alves
									Franco Brandão and Jaime Sichman</span></span></span></li>
				</ul>
			</ul>
			<ul>
				<li><strong style="line-height: 1.3em;">12:15 – 12:30 -</strong><span style="line-height: 1.3em;">&nbsp;
						<span style="line-height: 1.3em;"><span style="font-size: small;"><span
									style="line-height: normal;">Reputation in MAS-based Simulation: an approach using
									CArtAgO artifacts</span></span></span><br></span></li>
				<ul>
					<li><span style="line-height: 1.3em;"><span style="font-size: small;"><span
									style="line-height: normal;"><span style="line-height: normal;"><span
											style="line-height: normal;">Henrique Rodrigues, Glenda Dimuro, Graçaliz
											Dimuro, Diana Francisca Adamatti and Esteban
											Jerez</span></span></span></span></span></li>
				</ul>
			</ul>
			<p>&nbsp;</p>
			<table border="1" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td valign="top" width="688">
							<p align="center"><strong><span style="color: #999999;"><strong><strong><strong>Lunch
													12:30-14:30</strong></strong></strong></span><br></strong></p>
						</td>
					</tr>
				</tbody>
			</table>
			<p>&nbsp;</p>
			<table border="0" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td valign="top" width="690">
							<p align="center"><strong><strong><strong><strong>Invited Speaker
												2</strong></strong></strong></strong><strong></strong></p>
						</td>
					</tr>
				</tbody>
			</table>
			<p><strong>Chair:</strong> Rafael Bordini</p>
			<p><strong>14:30 - 15:30 Invited Speaker:</strong>&nbsp;Onn Shehory (IBM Haifa Research Lab)</p>
			<p><strong>Title:&nbsp;</strong><span style="color: #000000;"><span style="color: #000000;">Software
						Self-Healing: Improving the Quality of Complex Software Systems </span></span></p>
			<table border="0" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td valign="top" width="690">
							<p align="center"><strong><strong><strong>15:30-16:00: Technical </strong><strong>Session
											3<br></strong></strong></strong></p>
						</td>
					</tr>
				</tbody>
			</table>
			<p>&nbsp;<strong>Chair: </strong><span>Rafael Bordini</span></p>
			<ul>
				<li><strong style="line-height: 1.3em;">15:30 – 16:00</strong><span style="line-height: 1.3em;">
						-&nbsp;<span style="line-height: 1.3em;"><span style="font-size: small;"><span
									style="line-height: normal;"><span style="font-size: small;"><span
											style="line-height: normal;">Abordagem baseada em Agentes para o
											Monitoramento e Controle do Trabalho e Gestão Integrada de
											Mudanças</span></span></span></span></span></span></li>
				<ul>
					<li><span style="line-height: 1.3em;"><span style="font-size: small;"><span
									style="line-height: normal;"><span style="font-size: small;"><span
											style="line-height: normal;">Nécio De Lima Veras, Mariela Inés Cortés,
											Anderson Couto and Leandro Leocádio C. de
											Souza</span></span></span></span></span></li>
				</ul>
			</ul>
			<table style="width: 680px; height: 18px;" border="1" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td valign="top" width="688">
							<p align="center"><span style="color: #999999;"><strong><strong>Coffee break
											16:00-16:30</strong></strong></span></p>
						</td>
					</tr>
				</tbody>
			</table>
			<p>&nbsp;</p>
			<table border="0" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td style="text-align: center;" valign="top" width="690">
							&nbsp;<strong><strong><strong>16:30-17:10 Technical
									</strong><strong>Session</strong></strong> 4</strong></td>
					</tr>
				</tbody>
			</table>
			<p>&nbsp;<strong>Chair: </strong><span>Mariela Cortés<br></span></p>
			<ul>
				<li><strong style="line-height: 1.3em;">16:30 – 16:55</strong><span style="line-height: 1.3em;"> - <span
							style="font-size: small;"><span style="line-height: normal;"><span
									style="font-size: small;"><span style="line-height: normal;"><span
											style="line-height: 1.3em;"><span style="font-size: small;"><span
													style="line-height: normal;">Uma abordagem para o tratamento
													racional de normas de obrigação em ambientes de tarefas
													normativos</span></span></span></span></span></span></span></span>
				</li>
				<ul>
					<li><span style="line-height: 1.3em;"><span style="font-size: small;"><span
									style="line-height: normal;"><span style="font-size: small;"><span
											style="line-height: normal;"><span style="line-height: 1.3em;"><span
													style="font-size: small;"><span style="line-height: normal;"><span
															style="font-size: small;"><span
																style="line-height: normal;">Pedro Aragao, Mariela
																Cortés, Gustavo Campos and Francisco
																Cruz</span></span></span></span></span></span></span></span></span></span>
					</li>
				</ul>
			</ul>
			<ul>
				<li><strong style="line-height: 1.3em;">16:55 – 17:10 -</strong><span style="line-height: 1.3em;"> <span
							style="font-size: small;"><span style="line-height: normal;"><span
									style="font-size: small;"><span style="line-height: normal;">Adaptação de Artefatos
										de Software em Tempo de Execução utilizando Aspectos e
										Reflexão</span></span></span></span><br></span></li>
				<ul>
					<li><span style="line-height: 1.3em;"><span style="font-size: small;"><span
									style="line-height: normal;"><span style="font-size: small;"><span
											style="line-height: normal;">Samuel Davi Müller De Souza, F. J. Affonso and
											E. Y. Nakagawa</span></span></span></span></span></li>
				</ul>
			</ul>
			<table border="0" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td valign="top" width="690">
							<p align="center"><strong><strong><strong><strong>Panel</strong></strong></strong></strong>
							</p>
						</td>
					</tr>
				</tbody>
			</table>
			<p><strong>Chairs: </strong><span>Ingrid Nunes and Mariela Cortés<br></span></p>
			<ul>
				<li><strong style="line-height: 1.3em;">17:10 – 17:50</strong><span style="line-height: 1.3em;"> - <span
							style="font-size: small;"><span style="line-height: normal;"><span
									style="font-size: small;"><span style="line-height: 1.3em;">How to effectively
										develop decentralised agent-based
										solutions?<br></span></span></span></span></span></li>
				<ul>
					<li><span style="line-height: 1.3em;"><span style="font-size: small;"><span
									style="line-height: normal;"><span style="font-size: small;"><span
											style="line-height: normal;">Panelists: Jaime Sichman (USP) and Rafael
											Bordini (PUC-RS)</span></span></span></span></span></li>
				</ul>
			</ul>
			<table border="0" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td valign="top" width="690">
							<p align="center"><strong><strong><strong><strong>Closing and Announcement of Best
												Papers</strong></strong></strong><br></strong></p>
						</td>
					</tr>
				</tbody>
			</table>
			<p><strong><strong>Chair: </strong></strong>AutoSoft Organizers e Members of the AutoSoft Steering Committee
			</p>
		</div>
	</div>
</div>