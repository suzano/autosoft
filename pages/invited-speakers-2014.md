---
layout: page-2014
permalink: "invited-speakers-2014"
title: "Invited Speakers"
categories: "2014"
---

<div id="wrapper2">
	<div id="main" role="main">
		<div id="system-message-container">
		</div>
		<div class="item-page">
			<h2>
				Invited Speakers </h2>
			<p><img src="assets/img/mike_2014.jpg" border="0" alt="Michael Luck" width="170" height="199"
					style="float: left; margin-left: 10px; margin-right: 10px;"></p>
			<div>
				<h1><strong><a href="http://www.dcs.kcl.ac.uk/staff/mml/"
							target="_blank">Michael Luck <br></a></strong></h1>
				Michael Luck is Professor of Computer Science and Head of the School of Natural and Mathematical
				Sciences at King's College London, where he also works in the Agents and Intelligent Systems group,
				undertaking research into agent technologies and intelligent systems. He is Scientific Advisor to the
				Board for Aerogility.His work has sought to take a principled approach to the development of practical
				agent systems, and spans, among other areas, formal models for intelligent agents and multi-agent
				systems, norms and institutions, trust and reputation, application to bioinformatics and health, and
				deployment and technology forecasting. He is a director of the International Foundation for Autonomous
				Agents and Multi-Agent Systems (IFAAMAS), was a member of the Executive Committee of AgentLink III, the
				European Network of Excellence for Agent-Based Computing, having previously been the Director of
				AgentLink II. He is an editorial board member of Autonomous Agents and Multi-Agent Systems, the
				International Journal of Agent-Oriented Software Engineering, Web Intelligence and Agent Systems, and
				ACM Transactions on Autonomous and Adaptive Systems, as well as for the SpringerBriefs in Intelligent
				Systems series. He was also general co-chair of the Ninth International Conference on Autonomous Agents
				and Multiagent Systems (AAMAS 2010), held in Toronto, Canada in May 2010.
			</div>
			<div><br>
				<h2><span style="color: #000000;"><span style="color: #000000;"><span class="caption">From Agents to
								Electronic Order via Norms</span><br></span></span></h2>
				<span><strong>Abstract:</strong> Trust, reputation, norms and organisations are all relevant to the
					effective operation of open and dynamic multiagent systems. Inspired by human systems, yet not
					constrained by them, these concepts provide a means to establish a sense of order in computational
					environments (and mixed human-machine ones). In this talk I will review previous work across a range
					of areas in support of the need to develop theories and systems that provide the computational
					analogue of common social coordination mechanisms used by humans, in addition to those that might
					only find favour in computational systems. I will focus on particular examples that illustrate
					different approaches, including through the use of norms and contracts, and suggest some key
					challenges that need to be addressed to drive the field forward.<span
						class="caption"><br></span></span>
				<p><span><span class="caption"><br></span></span></p>
				<h1><img src="assets/img/onn_2014.jpg" border="0" alt="Onn Shehory" width="179"
						height="229" style="float: left; margin-left: 10px; margin-right: 10px;"><strong><a
							href="http://researcher.watson.ibm.com/researcher/view.php?person=il-ONN"
							target="_blank">Onn Shehory</a></strong></h1>
				Onn Shehory received his B.Sc. in physics and computer science in 1989, M.Sc. in physics in 1992, and
				Ph.D. in computer science in 1996, all from Bar Ilan University, Ramat Gan, Israel. He was a
				post-doctoral fellow and then a visiting assistant professor at Carnegie Mellon University, Pittsburgh,
				PA from 1996 to 1999. In 1999 he joined the IBM Haifa Research Lab as a research staff member, a
				position he holds to date. From 2000 to 2006 he was an adjunct lecturer and senior lecturer at the
				Technion, Israel Institute of Technology, Haifa, Israel. From 2002 to date he is an adjunct senior
				lecturer at Bar Ilan university, Ramat Gan, Israel. Dr. Shehory has edited 15 books and authored over 90
				articles. Among his research interests are autonomic systems, multi-agent systems, system and
				performance management, and software engineering. An article of his won the IFAAMAS influential Paper
				award, and two other papers of his won best paper awards. From 2006 to 2009 he served as a coordinator,
				and technical leader, of an international research project on software self-healing. Dr. Shehory is an
				associate editor of the International Journal on Autonomous Agents and Multi-Agent Systems, and of the
				International Journal on Agent Oriented Software Engineering. He was a board member of the International
				Foundation for Autonomous Agents and Multi-Agent Systems (IFAAMAS). Dr. Shehory served as program chair
				and general chair of multiple international conferences including the International Conference on
				Autonomic Computing and the international conference on Autonomous Agents and Multi-Agent
				Systems.<br><br><span><span><span class="caption"><br></span></span></span>
				<h2><span style="color: #000000;"><span style="color: #000000;">Software Self-Healing: Improving the
							Quality of Complex Software Systems </span></span></h2>
				<span><span class="caption"><span><strong>Abstract:</strong> Large and complex software systems are
							error-prone. Testing and debugging technologies do not fully address this problem. Hence,
							software products released to the market are inherently faulty. To overcome this problem,
							there is a need for a paradigm shift in software quality methodologies. In this respect, an
							emerging approach is to provide the software itself with autonomic properties that can
							overcome its faults and failures. Software self-awareness, self-diagnosis and self-healing
							aim to provide the sought cababilities. Software self-healing solutions deployed to date
							commonly address a single class of problems. Broader self-healing solutions are usually not
							applicable in industrial systems. Recently, some attempts were made to address the need for
							industry-grade, generic software self-healing. In this talk I present self-healing in
							general as well as specific implemented cases. I aim to examine advantages, weaknesses, wide
							applicability, and open research issues.<br><br></span></span></span>
			</div>
		</div>
	</div>
</div>