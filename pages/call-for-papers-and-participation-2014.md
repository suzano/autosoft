---
layout: page-2014
permalink: "call-for-papers-and-participation-2014"
title: "Call for papers and participation"
categories: "2014"
---

<div id="wrapper2">
	<div id="main" role="main">
		<div id="system-message-container">
		</div>
		<div class="item-page">
			<h2>
				Call for papers and participation </h2>
			<p><strong>5th Workshop on Autonomous Software Systems at CBSoft&nbsp;</strong></p>
			<p>Call for Papers in PDF: <a
					href="http://inf.ufrgs.br/prosoft/autosoft2014/autosoft2014-CFP.pdf">AutoSoft
					2014 - CFP.pdf</a><strong><a
						href="http://inf.ufrgs.br/~ingridnunes/temp/autosoft2014-CFP.pdf"></a><br></strong>
			</p>
			<p><strong>Motivation</strong></p>
			<p>Many systems no longer depend on the traditional human operator to achieve some or all of the complex
				properties and adaptable behaviour patterns necessary to meet demanding operational situations. Prime
				drivers for this shift toward systems that comprise minimal or no human element are the benefits of
				enhanced system performance, reduced life cycle costs and the ability to operate in hazardous or remote
				environments.</p>
			<p>In this sense, autonomous systems represent the next great step in the fusion of computing, sensing, and
				software to create intelligent systems capable of interacting with the complexities of the real world.
				The transition from passive to autonomous components turned software architectures into complex
				platforms that may contain many components that dynamically interact, and engage in complex coordination
				protocols. Transportation, aerospace, defense and finance are amongst some of the sectors shifting
				towards replacing the human operator by architectures and implementation technologies that are intended
				to provide overall system improvements.</p>
			<p><strong>Goals</strong></p>
			<p>An important goal of this workshop is to extend the state of the art in both engineering and application
				in the context of autonomous systems. The main idea is to bring together researchers and practitioners,
				involved in the development of techniques and applications of Autonomous Software Systems, to discuss
				the latest developments, trends and innovations concerning the theory and practice of such software.</p>
			<p><strong>Topics of Interest</strong></p>
			<p>The workshop will encourage submissions related to all topics in the area of Autonomous Software Systems,
				including (but not limited to) the following:</p>
			<ul>
				<li><span style="line-height: 1.3em;">Agent systems and agent-oriented software engineering</span></li>
				<li><span style="line-height: 1.3em;">Autonomous systems simulation</span></li>
				<li><span style="line-height: 1.3em;">Domain-specific applications of autonomous software systems</span>
				</li>
				<li><span style="line-height: 1.3em;">Embedded systems</span></li>
				<li><span style="line-height: 1.3em;">Emergent intelligence for autonomous systems</span></li>
				<li><span style="line-height: 1.3em;">Games and autonomous applications</span></li>
				<li><span style="line-height: 1.3em;">Industry applications of autonomous software systems</span></li>
				<li><span style="line-height: 1.3em;">Infrastructures and tools for autonomous systems</span></li>
				<li><span style="line-height: 1.3em;">Motion planning and scheduling</span></li>
				<li><span style="line-height: 1.3em;">Ontologies for autonomous applications</span></li>
				<li><span style="line-height: 1.3em;">Robotics and automation applications</span></li>
				<li><span style="line-height: 1.3em;">Safety in autonomous software systems</span></li>
				<li><span style="line-height: 1.3em;">Semantics for autonomous negotiation, orchestration, composition
						and execution</span></li>
				<li><span style="line-height: 1.3em;">Service-oriented computing and autonomous software systems</span>
				</li>
				<li><span style="line-height: 1.3em;">Trust in autonomous societies</span></li>
				<li><span style="line-height: 1.3em;">Unmanned systems technology</span></li>
				<li><span style="line-height: 1.3em;">Verification of autonomous systems</span></li>
			</ul>
		</div>
	</div>
</div>