---
layout: page-2014
permalink: "submission-guidelines-2014"
title: "Submission Guidelines"
categories: "2014"
---

<div id="wrapper2">
	<div id="main" role="main">
		<div id="system-message-container">
		</div>
		<div class="item-page">
			<h2>
				Submission Guidelines </h2>
			<p>Authors are invited to submit either (i) technical, original papers that should not have been published
				or submitted to other workshops, &nbsp;journals or book chapters; or (ii) position papers. We
				encourage&nbsp;authors to present novel ideas, critique of existing work, and &nbsp;practical studies
				and experiments related to autonomous software systems.</p>
			<p>Both technical and position papers should be submitted according to the <a
					href="http://www.sbc.org.br/index.php?option=com_jdownloadsItemid=195task=view.downloadcatid=32cid=38">SBC
					format</a>. <strong>Technical papers</strong> should have at least 10 pages and must not
				&nbsp;exceed 12, and <strong>position papers</strong> should have at least 4 pages and must not exceed
				6. These limits are for A4 pages including all text, references, appendices, and figures. Submitted
				papers can be written in Portuguese or English and must be in Adobe Portable Document Format (PDF).
				Please follow the guidelines established by the Brazilian Computer Society. S<em>ubmissions should
					indicate their eligibility for the best student paper and best undergraduate student paper <a
						href="awards-2014">awards</a>.</em>
			</p>
			<p>Submitted papers will be reviewed by at least three reviewers.&nbsp;Accepted papers will be published in
				conjunction of the CBSoft&nbsp;proceedings. Furthermore, the publication of a paper is subject to the
				registration of at least one of its authors to present it in the&nbsp;workshop.</p>
			<p>Submissions must be via EasyChair at: <a
					href="https://www.easychair.org/conferences/?conf=autosoft2014">https://www.easychair.org/conferences/?conf=autosoft2014</a>
			</p>
			<p>If you have any further questions, please email us at&nbsp;
				<a href="mailto:autosoft2014@easychair.org">autosoft2014@easychair.org</a>
			</p>
		</div>
	</div>
</div>