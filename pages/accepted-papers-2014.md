---
layout: page-2014
permalink: "accepted-papers-2014"
title: "Accepted Papers"
categories: "2014"
---

<div id="wrapper2">
	<div id="main" role="main">
		<div id="system-message-container">
		</div>
		<div class="item-page">
			<h2>
				Accepted Papers </h2>
			<p><span style="font-size: small;"><span style="line-height: normal;"><span
							style="text-decoration: underline;"><strong>Full Papers:</strong></span><br></span></span>
			</p>
			<ul>
				<li><span style="font-size: small;"><span style="line-height: normal;">Pedro Aragao, Mariela Cortés,
							Gustavo Campos and Francisco Cruz. Uma abordagem para o tratamento racional de normas de
							obrigação em ambientes de tarefas normativos</span></span></li>
				<li><span style="font-size: small;"><span style="line-height: normal;">Ingrid Nunes and Alberto
							Schaeffer-Filho. Reengineering Network Resilience Strategies using the BDI
							Architecture</span></span></li>
				<li><span style="font-size: small;"><span style="line-height: normal;">Tiago Luiz Schmitz and Jomi Fred
							Hubner. Uma plataforma para agentes normativos baseada no modelo Huginn</span></span></li>
				<li><span style="font-size: small;"><span style="line-height: normal;">Nécio De Lima Veras, Mariela Inés
							Cortés, Anderson Couto and Leandro Leocádio C. de Souza. Abordagem baseada em Agentes para o
							Monitoramento e Controle do Trabalho e Gestão Integrada de Mudanças</span></span></li>
				<li><span style="font-size: small;"><span style="line-height: normal;">Robert Marinho, Emmanuel Sávio
							Silva Freire and Mariela Inés Cortés. JAMDER 2.0: Support to Implementation of Normative
							Multi-Agent Systems</span></span></li>
				<li><span style="font-size: small;"><span style="line-height: normal;">Felipe Cardoso Resende, Sara
							Casare, Anarosa Alves Franco Brandão and Jaime Sichman. Fragmenting O-MaSE using the Medee
							Framework</span></span></li>
			</ul>
			<p><span style="font-size: small;"><span style="line-height: normal;"><br><span
							style="text-decoration: underline;"><strong>Short Papers:</strong></span><br></span></span>
			</p>
			<ul>
				<li><span style="font-size: small;"><span style="line-height: normal;">Henrique Rodrigues, Glenda
							Dimuro, Graçaliz Dimuro, Diana Francisca Adamatti and Esteban Jerez. Reputation in MAS-based
							Simulation: an approach using CArtAgO artifacts</span></span></li>
				<li><span style="font-size: small;"><span style="line-height: normal;">Samuel Davi Müller De Souza, F.
							J. Affonso and E. Y. Nakagawa. Adaptação de Artefatos de Software em Tempo de Execução
							utilizando Aspectos e Reflexão</span></span></li>
			</ul>
			<p><span style="font-size: small;"><span style="line-height: normal;"><br></span></span></p>
		</div>
	</div>
</div>