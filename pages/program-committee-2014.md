---
layout: page-2014
permalink: "program-committee-2014"
title: "Program Committee"
categories: "2014"
---

<div id="wrapper2">
	<div id="main" role="main">
		<div id="system-message-container">
		</div>
		<div class="item-page">
			<h2>
				Program Committee </h2>
			<p>Ana L. C. Bazzan (Universidade Federal do Rio Grande do Sul)<br>Ana Paula Lemke (IFRS-Campus
				Feliz)<br>Anarosa Alves Franco Brandão (University of Sao Paulo)<br>Andrea Omicini (Alma Mater
				Studiorum–Università di Bologna)<br>Baldoino Fonseca (Federal University of Alagoas)<br>Carlos Lucena
				(PUC-Rio)<br>Danny Weyns (Linnaeus University)<br>Denis Wolf (University of Sao Paulo)<br>Diana
				Francisca Adamatti (Universidade Federal do Rio Grande)<br>Elder Cirilo (UFSJ)<br>Felipe Meneguzzi
				(Pontifical Catholic University of Rio Grande do Sul)<br>Guillermo Ricardo Simari (Universidad Nacional
				del Sur in Bahia Blanca)<br>Gustavo Campos (Universidade Estadual do Ceará)<br>Gustavo Giménez-Lugo
				(Federal University of Technology-Paraná (UTFPR))<br>Ingrid Nunes (UFRGS)<br>Jaime Sichman (University
				of Sao Paulo)<br>Joao Leite (CENTRIA, Universidade Nova de Lisboa)<br>Jomi Fred Hubner (Federal
				University of Santa Catarina)<br>Kalinka Regina Castelo Branco (USP)<br>Luis Gustavo Nardin (University
				of São Paulo)<br>Maira Gatti (IBM Research Brazil)<br>Marcelo Ribeiro (IEEE Member)<br>Mariela Cortés
				(UECE)<br>Olivier Boissier (ENS Mines Saint-Etienne)<br>Paulo A. L. Castro (ITA&nbsp; - Technological
				Institute of Aeronautics)<br>Rafael H. Bordini (FACIN-PUCRS)<br>Raimundo Barreto (UFAM)<br>Renato
				Cerqueira (Computer Science Department, PUC-Rio)<br>Rodrigo Paes (Federal University of
				Alagoas)<br>Samhar Mahmoud (King's College London)<br>Valdinei Silva (Universidade de São
				Paulo)<br>Valguima Victoria Viana Aguiar Odakura (Universidade Federal da Grande Dourados)<br>Wamberto
				Vasconcelos (Department of Computing Science, University of Aberdeen)<br>Zahia Guessoum (LIP6, Universit
				de Paris 6)</p>
		</div>
	</div>
</div>