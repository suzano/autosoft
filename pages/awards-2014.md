---
layout: page-2014
permalink: "awards-2014"
title: "Awards"
categories: "2014"
---

<div id="wrapper2">
	<div id="main" role="main">
		<div id="system-message-container">
		</div>
		<div class="item-page">
			<h2>
				Awards </h2>
			<p>The authors of the best papers will be invited to prepare extended versions of their papers after the
				workshop. The extended papers will go through an additional round of review.</p>
			<p>The following awards will be given:</p>
			<ul>
				<li><strong><span style="line-height: 1.3em;">Best Paper Award</span></strong></li>
				<ul>
					<li><span style="line-height: 1.3em;">Extended version will appear in the <a
								href="http://seer.ufrgs.br/rita">Revista de
								Informática Teórica e Aplicada – RITA</a></span></li>
					<li><span style="line-height: 1.3em;">All papers are eligible</span></li>
				</ul>
				<li><strong><span style="line-height: 1.3em;">Best Student Paper Award</span></strong></li>
				<ul>
					<li><span style="line-height: 1.3em;">Extended version will appear in the <a
								href="http://seer.ufrgs.br/rita">Revista de
								Informática Teórica e Aplicada – RITA</a></span></li>
					<li><span style="line-height: 1.3em;">Only papers whose first author is a student are
							eligible</span></li>
				</ul>
				<li><strong><span style="line-height: 1.3em;">Best Undergraduate Student Paper Award</span></strong>
				</li>
				<ul>
					<li><span style="line-height: 1.3em;">Extended version will appear in the <a
								href="http://seer.ufrgs.br/reic/">Revista
								Eletrônica de Iniciação Científica (REIC)</a></span></li>
					<li><span style="line-height: 1.3em;">Only papers whose first author is an undergraduate student are
							eligible</span></li>
				</ul>
			</ul>
			<p><em><span style="line-height: 1.3em;">Submissions should indicate their eligibility for the
						award.</span></em></p>
			<p><em><span style="line-height: 1.3em;">Members of the steering committee of the AutoSoft are responsible
						for choosing best papers, based on reviews made by PC members. Members that have candidate
						papers for the awards do not participate of the decision.</span></em></p>
			<h1><em><span style="line-height: 1.3em;">Best Paper Awards 2014<br></span></em></h1>
			<p><span style="line-height: 1.3em;"><span style="line-height: 1.3em;"><strong>Best Undergraduate Student
							Paper Award</strong><br>Title: Uma abordagem para o tratamento racional de normas de
						obrigação em ambientes de tarefas normativos<br>Authors: Pedro Aragao, Mariela Cortés, Gustavo
						Campos and Francisco Cruz</span><br><br><strong>Best Student Paper Award</strong><br><span
						style="line-height: 1.3em;"><span style="line-height: 1.3em;">Title</span></span>: Uma
					plataforma para agentes normativos baseada no modelo Huginn<br><span
						style="line-height: 1.3em;"><span style="line-height: 1.3em;">Authors</span></span>: Tiago Luiz
					Schmitz and Jomi Fred Hubner<br></span></p>
			<p><span style="line-height: 1.3em;"><strong>Best Paper Award</strong><br><span
						style="line-height: 1.3em;"><span style="line-height: 1.3em;">Title</span></span>: Reengineering
					Network Resilience Strategies using the BDI Architecture<br><span style="line-height: 1.3em;"><span
							style="line-height: 1.3em;">Authors</span></span>:Ingrid Nunes and Alberto
					Schaeffer-Filho</span></p>
			<p><em><span style="line-height: 1.3em;"><br></span></em></p>
		</div>
	</div>
</div>