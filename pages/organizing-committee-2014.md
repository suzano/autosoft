---
layout: page-2014
permalink: "organizing-committee-2014"
title: "Organizing Committee"
categories: "2014"
---

<div id="wrapper2">
	<div id="main" role="main">
		<div id="system-message-container">
		</div>
		<div class="item-page">
			<h2>
				Organizing Committee </h2>
			<p><strong>Alphabetical list</strong></p>
			<p><strong><strong>Workshop Chairs</strong></strong></p>
			<ul>
				<li><span style="line-height: 1.3em;"><a
							href="http://inf.ufrgs.br/~ingridnunes/">Ingrid
							Nunes</a> (UFRGS, Brazil)</span></li>
				<li><span style="line-height: 1.3em;"><a
							href="http://lattes.cnpq.br/1745038503462998">Mariela&nbsp;Cortés</a>
						(UECE, Brazil)</span></li>
			</ul>
			<div>
				<p><strong><strong><strong>Program Chair</strong></strong></strong></p>
				<ul>
					<li>
						<p>Ingrid Nunes (UFRGS, Brazil)</p>
					</li>
				</ul>
			</div>
			<p><strong style="line-height: 1.3em;"><strong><strong><strong>Steering
								Committee</strong></strong></strong></strong></p>
			<ul>
				<li><span style="line-height: 1.3em;">Anarosa A.F. Brandão (USP, Brazil)</span></li>
				<li><span style="line-height: 1.3em;">Carlos Lucena, PUC-Rio (RJ, Brazil)</span></li>
				<li><span style="line-height: 1.3em;">Jaime Simão Sichman, Poli/USP (SP, Brazil)</span></li>
				<li><span style="line-height: 1.3em;">Rafael Bordini, UFRGS (RS, Brazil)&nbsp;</span></li>
				<li><span style="line-height: 1.3em;">Ricardo Choren, IME – RJ, Brazil&nbsp;</span></li>
				<li><span style="line-height: 1.3em;">Viviane Torres da Silva (UFF, Brazil)</span></li>
			</ul>
			<p><strong><strong><strong><strong><strong>Contact</strong></strong></strong></strong></strong></p>
			<ul>
				<li><span style="line-height: 1.3em;">Ingrid Nunes &lt;</span><span
						style="line-height: 1.3em;">ingridnunes AT inf.ufrgs.br&gt;</span></li>
				<li><span>Mariela&nbsp;Cortés &lt;mariela AT larces.uece.br&gt;</span></li>
			</ul>
		</div>
	</div>
</div>