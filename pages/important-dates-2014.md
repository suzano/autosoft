---
layout: page-2014
permalink: "important-dates-2014"
title: "Important Dates"
categories: "2014"
---

<div id="wrapper2">
	<div id="main" role="main">
		<div id="system-message-container">
		</div>
		<div class="item-page">
			<h2>
				Important Dates </h2>
			<table style="width: 387px; height: 94px;">
				<tbody>
					<tr>
						<td>
							<ul>
								<li><strong>Paper Submission</strong></li>
							</ul>
						</td>
						<td><strong>&nbsp;June 29, 2014 (Anywhere on Earth)<br></strong></td>
					</tr>
					<tr>
						<td>
							<ul>
								<li><strong>Author Notification </strong></li>
							</ul>
						</td>
						<td><strong>&nbsp;July 17, 2014</strong></td>
					</tr>
					<tr>
						<td>
							<ul>
								<li><strong>Camera Ready</strong></li>
							</ul>
						</td>
						<td><strong>&nbsp;July 24, 2014</strong></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>